import { ref } from 'vue';
import Todo from './Todo'; // Import the Todo component

export default {
  components: {
    Todo
  },

  setup() {
    const tasks = ref([
      {
        id: 0,              // Initialize id to a suitable starting value
        title: '',
        taskName: '',
        time: '00:00',
        showTimePicker: false,
        completed: false,
      },
    ]);

    const addTask = () => {
      const id = tasks.value.length;
      tasks.value.push({
        id: id,
        title: '',
        taskName: '',
        time: '00:00',
        showTimePicker: false,
        completed: false,
      });
      console.log(id);
    };

    const deleteTask = (index) => {
      tasks.value.splice(index, 1);
    };

    const cancelTimeSelection = (index) => {
      tasks.value[index].time = tasks.value[index].selectedTime || '00:00';
      tasks.value[index].showTimePicker = false;
    };

    const saveTimeSelection = (index) => {
      tasks.value[index].selectedTime = tasks.value[index].time;
      tasks.value[index].showTimePicker = false;
    };

    const cancelTask = () => {
      tasks.value = [
        {
          id: tasks.value.length,  // Assign a new id for the canceled task
          title: '',
          taskName: '',
          time: '00:00',
          showTimePicker: false,
          completed: false,
        },
      ];
    };

    const saveTask = () => {
      console.log("button clicked");
    }

    return {
      tasks,
      addTask,
      deleteTask,
      cancelTimeSelection,
      saveTimeSelection,
      cancelTask,
      saveTask,
    };
  },
};
