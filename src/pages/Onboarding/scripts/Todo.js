import { ref, computed, defineProps } from 'vue';

export default {
  setup(props) {
    const expandedProgress = ref(false);
    const expandedDone = ref(false);
    const text = ref(null);
    const creationTime = ref(new Date());
    const teal = ref(false);

    const currentDate = computed(() => {
      const now = new Date();
      const options = { day: 'numeric', month: 'long', year: 'numeric' };
      return now.toLocaleDateString('en-US', options);
    });

    const formattedCreationTime = computed(() => {
      const options = { hour: 'numeric', minute: 'numeric' };
      return creationTime.value.toLocaleTimeString('en-US', options);
    });

    const toggleExpandedProgress = () => {
      expandedProgress.value = !expandedProgress.value;
    };
    const toggleExpandedDone = () => {
      expandedDone.value = !expandedDone.value;
    };


    return {
      expandedProgress,
      expandedDone,
      text,
      creationTime,
      teal,
      currentDate,
      formattedCreationTime,
      toggleExpandedProgress,
      toggleExpandedDone,
    };
  },
};
